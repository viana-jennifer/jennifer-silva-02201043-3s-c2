package br.com.bandtec.ac2pweb.repository;

import br.com.bandtec.ac2pweb.domain.Lutador;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LutadorRepository extends JpaRepository<Lutador, Integer> {


}
