package br.com.bandtec.ac2pweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ac2PwebApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ac2PwebApplication.class, args);
	}

}
